package gov;
import gov.ts.Cars;
import gov.ts.Moto;
import gov.ts.Truck;
import gov.ts.Bus;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadWrite extends CarInfo{
static String Del="|";

ArrayList<CarInfo> ListCar=new ArrayList<CarInfo>();

public void Delete(int NumDel) {
    int DelIn=NumDel-1;
    String WhatDel=ListCar.get(DelIn).Num;
   ListCar.remove(DelIn);
int max=ListCar.size()-1;


   try(RandomAccessFile RandomAdd = new RandomAccessFile("src/gov/data.txt", "rw")) {

                  String Pr;
                  String Pr2;
        for(int i=0; i<RandomAdd.length(); i++) {

            RandomAdd.seek(i);
            byte[] bytes = new byte[8];
            RandomAdd.read(bytes);
            Pr=new String(bytes);

            if (Pr.equals(WhatDel)){
                RandomAdd.seek(i-1);
                for(int j=0; j<69; j++) {
                RandomAdd.write(' ');
                            }
                for(int u=i-1; u<RandomAdd.length(); u++) {
                    RandomAdd.seek(u);
                    byte[] bytes1 = new byte[8];
                    RandomAdd.read(bytes1);
                    Pr2 = new String(bytes1);

                    if (Pr2.equals(ListCar.get(max).Num)) {
                        RandomAdd.seek(u - 1);
                        for (int j = 0; j < 69; j++) {
                            RandomAdd.write(' ');
                        }
                    }
                }

                    RandomAdd.seek(i - 1);

                    String whatWrite = Del + ListCar.get(max).Num + Del + ListCar.get(max).Vin + Del + ListCar.get(max).Vehicle + Del + ListCar.get(max).Owner + Del + ListCar.get(max).Mass + Del + ListCar.get(max).Type + Del;
                    int Le = whatWrite.length();


                    for (int y = 0; y < (70 - Le); y++) {
                        whatWrite = whatWrite + " ";
                    }
                    RandomAdd.write(whatWrite.getBytes());
                }


        }
       RandomAdd.close();
       ListCar.remove(max);
   }
   catch(IOException ex) {
       System.out.println(ex.getMessage());
   }
}


public void editCar(int Ed){
    int DelIn=Ed-1;
    String WhatDel=ListCar.get(DelIn).Num;

    String InputS;
    Scanner inp=new Scanner(System.in);
    System.out.print('\n'+ListCar.get(DelIn).Num+" - Введите новый номер: ");
    InputS=inp.nextLine();
    ListCar.get(DelIn).Num=InputS;
    System.out.print('\n'+ListCar.get(DelIn).Owner+" - Введите нового владельца: ");
    InputS=inp.nextLine();
    ListCar.get(DelIn).Owner=InputS;



    try(RandomAccessFile RandomAdd = new RandomAccessFile("src/gov/data.txt", "rw")) {
        String Pr;
        for(int i=0; i<RandomAdd.length(); i++) {
            RandomAdd.seek(i);
            byte[] bytes = new byte[8];
            RandomAdd.read(bytes);
            Pr=new String(bytes);
                if (Pr.equals(WhatDel)){
                RandomAdd.seek(i-1);
                for(int j=0; j<69; j++) {
                    RandomAdd.write(' ');
                }
                    RandomAdd.seek(i-1);
                    String whatWrite=Del+ListCar.get(DelIn).Num+Del+ListCar.get(DelIn).Vin+Del+ListCar.get(DelIn).Vehicle+Del+ListCar.get(DelIn).Owner+Del+ListCar.get(DelIn).Mass+Del+ListCar.get(DelIn).Type+Del;
                    int Le=whatWrite.length();
                    for(int y=0; y<(70-Le); y++){
                        whatWrite=whatWrite+" ";
                    }
                    RandomAdd.write(whatWrite.getBytes());

            }}
        RandomAdd.close();
    }
    catch(IOException ex) {
        System.out.println(ex.getMessage());
    }
}

    public void WriteTXT() {
       // int Size;
        CarInfo addVehicle=new CarInfo();
        int x=(int)(Math.random()*4)+1;
        switch(x) {
            case 1: {
                addVehicle= new Cars();
                            break;
            }
            case 2: {
                addVehicle = new Moto();
                               break;
            }
            case 3: {
                addVehicle = new Truck();
                             break;
            }
            case 4: {
                addVehicle = new Bus();
                               break;
            }
        }
        try (FileWriter writer = new FileWriter("src/gov/data.txt", true)) {
            String whatWrite=Del+addVehicle.Num+Del+addVehicle.Vin+Del+addVehicle.Vehicle+Del+addVehicle.Owner+Del+addVehicle.Mass+Del+addVehicle.Type+Del;
            int Le=whatWrite.length();
            for(int y=0; y<(70-Le); y++){
                whatWrite=whatWrite+" ";
            }
            writer.write(whatWrite+'\n');
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
}

    public void ReadTXT(){
ListCar.clear();
      try(  FileReader reader = new FileReader("src/gov/data.txt")) {
          Scanner scan = new Scanner(reader);
          int i = 0;
          int t=0;
          while (scan.hasNextLine()) {
              String readLine = scan.nextLine();
              char[] Chr=readLine.toCharArray();
              if (Chr[0]!='|'){
                  continue;
              }
                  ListCar.add(new CarInfo());
              t=0;
              ListCar.get(i).Num="";
              ListCar.get(i).Vin="";
              ListCar.get(i).Vehicle="";
              ListCar.get(i).Owner="";
              ListCar.get(i).Mass="";
              ListCar.get(i).Type="";
                for(int k=0; k<readLine.length(); k++) {
                if(Chr[k]=='|'){
                    t++;
                }
                else {
                    switch (t) {
                        case 1: {
                            ListCar.get(i).Num=ListCar.get(i).Num+Chr[k];
                            break;
                        }
                        case 2: {
                            ListCar.get(i).Vin=ListCar.get(i).Vin+Chr[k];
                            break;
                        }
                        case 3: {
                            ListCar.get(i).Vehicle=ListCar.get(i).Vehicle+Chr[k];
                            break;
                        }
                        case 4: {
                            ListCar.get(i).Owner=ListCar.get(i).Owner+Chr[k];
                            break;
                        }
                        case 5: {
                            ListCar.get(i).Mass=ListCar.get(i).Mass+Chr[k];
                           // Ms=Ms+Chr[k];
                            break;
                        }
                        case 6: {
                            ListCar.get(i).Type=ListCar.get(i).Type+Chr[k];
                            break;
                        }
                    }
                }
              }
                i++;
          }
          reader.close();
              System.out.printf("%-19s", "Гос. номер");
              System.out.printf("%-20s", "VIN");
              System.out.printf("%-15s", "Марка");
              System.out.printf("%-25s", "Хозяин");
              System.out.printf("%-15s", "Масса");
              System.out.printf("%-10s%n", "Тип транспорта");
              System.out.println("______________________________________________________________________________________________________________");
              for (i = 0; i < ListCar.size(); i++) {
                 if (i<9){
                  System.out.print(" "+(i+1)+": ");}
                 else
                 {
                     System.out.print((i+1)+": ");
                 }
                  ListCar.get(i).ShowInfo();
              }
                    }
      catch (IOException ex){
          System.out.println(ex.getMessage());
      }
    }
}
