package gov.ts;

import gov.CarInfo;

import java.io.Serializable;

public class Truck extends CarInfo implements Serializable {
    String[] Model={"Mercedes", "Man", "Volvo"};
    public Truck()
    {
        super.CreateInfo();
        Vehicle = Model[(int)(Math.random()*3)];
        long VinN = (long) (Math.random() * 100000000);
        Vin = "T"+VinN+"L";
        Mass=Integer.toString(3000+(int)(Math.random()*2000));
        Type="Gruzovoy";
    }



}
