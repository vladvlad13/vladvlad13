package gov.ts;

import gov.CarInfo;

import java.io.Serializable;

public class Moto extends CarInfo implements Serializable {


    String[] Model={"Suzuki", "Kawasaki", "Harley", "Ducati", "KTM"};
    public Moto()
    {
        super.CreateInfo();
        Vehicle = Model[(int)(Math.random()*5)];
        long VinN = (long) (Math.random() * 1000000);
        Vin = "MJ0"+VinN;
        Mass=Integer.toString(150+(int)(Math.random()*100));
        Type="Moto";
    }
}
